#ifndef RECAST_H
#define RECAST_H

#include <QObject>

class Recast : public QObject
{
    Q_OBJECT
public:
    explicit Recast(QObject *parent = nullptr);
    Q_PROPERTY(QString hostName MEMBER m_hostname NOTIFY hostNameChanged)

signals:
    void hostNameChanged();

public slots:

private:
    QString m_hostname;
};

#endif // RECAST_H
