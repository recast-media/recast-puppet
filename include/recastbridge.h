#ifndef RECASTBRIDGE_H
#define RECASTBRIDGE_H

#include <QObject>
#include <QtWebSockets>

class RecastBridge : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool canNext MEMBER m_canNext NOTIFY updated)
    Q_PROPERTY(bool canPrevious MEMBER m_canPrevious NOTIFY updated)
    Q_PROPERTY(bool canPause MEMBER m_canPause NOTIFY updated)
    Q_PROPERTY(bool canStop MEMBER m_canStop NOTIFY updated)
    Q_PROPERTY(bool canPlay MEMBER m_canPlay NOTIFY updated)
    Q_PROPERTY(bool canSeek MEMBER m_canSeek NOTIFY updated)
    Q_PROPERTY(double mediaDuration MEMBER m_mediaDuration NOTIFY updated)
    Q_PROPERTY(double currentMediaPosition MEMBER m_currentMediaPosition NOTIFY updated)
    Q_PROPERTY(QString currentMediaName MEMBER m_currentMediaName NOTIFY updated)
    Q_PROPERTY(QString currentMediaCreator MEMBER m_currentMediaCreator NOTIFY updated)
    Q_PROPERTY(QUrl currentMediaArt MEMBER m_currentMediaArt NOTIFY updated)

public:
    explicit RecastBridge(QObject *parent = nullptr);

signals:
    void next();
    void previous();
    void pause();
    void togglePlayPause();
    void stop();
    void play();
    void seek(double offset);
    void jumpToTime(double time);

    void reset();
    void preparePayload();
    void payloadReceived();
    void payloadLoaded();

    void updated();

public slots:

private:
    bool m_canNext;
    bool m_canPrevious;
    bool m_canPause;
    bool m_canStop;
    bool m_canPlay;
    bool m_canSeek;
    double m_mediaDuration;
    double m_currentMediaPosition;
    QString m_currentMediaName;
    QString m_currentMediaCreator;
    QUrl m_currentMediaArt;

    QWebSocketServer *m_webSocketServer;
    QList<QWebSocket*> m_clients;

    QByteArray m_currentData;
    QList<QByteArray> m_incomingPayload;

    void newConnection();
    void processText(QString);
    void processBinary(QByteArray);
    void socketDisconnected();
};

#endif // RECASTBRIDGE_H
