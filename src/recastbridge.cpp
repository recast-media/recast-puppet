#include "recastbridge.h"

#define comp(__val__) (message == QStringLiteral(__val__))
#define property(__val__, __property__) else if (message == QStringLiteral(__val__)) { \
    sendProperty(__val__, __property__); \
    }
#define sendProperty(__val__, __property__) client->sendTextMessage(QStringLiteral(__val__) + QStringLiteral(" ") + QVariant(__property__).toString())
#define runsignal(__val__, __signal__) else if (message == QStringLiteral(__val__)) { \
    emit this->__signal__(); \
    }

RecastBridge::RecastBridge(QObject *parent) : QObject(parent)
{
    m_webSocketServer = new QWebSocketServer(QStringLiteral("Recast Puppet"), QWebSocketServer::NonSecureMode, this);
    if (m_webSocketServer->listen(QHostAddress::Any, 20020)) {
        qDebug() << "listening on port 20020 for messages";
        connect(m_webSocketServer, &QWebSocketServer::newConnection,
                this, &RecastBridge::newConnection);
    }
    QObject::connect(this, &RecastBridge::reset, this, [this]() {
        this->m_canNext = false;
        this->m_canPrevious = false;
        this->m_canPause = false;
        this->m_canStop = false;
        this->m_canPlay = false;
        this->m_canSeek = false;
        this->m_mediaDuration = -1;
    });
    emit this->reset();
    QObject::connect(this, &RecastBridge::updated, this, [this]() {
       for (auto client : this->m_clients) {
           sendProperty("canNext", m_canNext);
           sendProperty("canPrevious", m_canPrevious);
           sendProperty("canPause", m_canPause);
           sendProperty("canStop", m_canStop);
           sendProperty("canPlay", m_canPlay);
           sendProperty("canSeek", m_canSeek);
           sendProperty("mediaDuration", m_mediaDuration);
           sendProperty("currentMediaPosition", m_currentMediaPosition);
           sendProperty("currentMediaName", m_currentMediaName);
           sendProperty("currentMediaCreator", m_currentMediaCreator);
           sendProperty("currentMediaArt", m_currentMediaArt);
       }
    });
    QObject::connect(this, &RecastBridge::preparePayload, this, [this]() {
        m_incomingPayload.clear();
    });
    QObject::connect(this, &RecastBridge::payloadReceived, this, [this]() {
        QByteArray concatted;
        for (auto array : m_incomingPayload) {
            concatted += array;
        }

        {
            unsigned char* raw = reinterpret_cast<unsigned char*>(m_currentData.data());
            QResource::unregisterResource(raw, QStringLiteral("/app"));
        }

        m_currentData = concatted;

        unsigned char* raw = reinterpret_cast<unsigned char*>(m_currentData.data());

        if (QResource::registerResource(raw, QStringLiteral("/app"))) {
            emit this->payloadLoaded();
        }
    });
}

void RecastBridge::newConnection()
{
    QWebSocket *socket = m_webSocketServer->nextPendingConnection();

    connect(socket, &QWebSocket::textMessageReceived, this, &RecastBridge::processText);
    connect(socket, &QWebSocket::binaryMessageReceived, this, &RecastBridge::processBinary);
    connect(socket, &QWebSocket::disconnected, this, &RecastBridge::socketDisconnected);

    m_clients << socket;
}

void RecastBridge::processText(QString message)
{
    qDebug() << message;
    QWebSocket *client = qobject_cast<QWebSocket *>(sender());
    QFile data("qrc:///qtwebchannel/qwebchannel.js");
    if (data.open(QIODevice::ReadOnly)) {
        qDebug() << QString(data.readAll());
    }
    if comp("payload") {
        emit this->preparePayload();
        client->sendTextMessage(QStringLiteral("ready"));
    } else if comp("payload_done") {
        emit this->payloadReceived();
        client->sendTextMessage(QStringLiteral("finished"));
    } else if (message.startsWith("seek")) {
        if (auto split = message.split(" "); split.length() >= 2) {
            emit this->seek(QVariant(split[1]).toDouble());
        }
    } else if (message.startsWith("jumpToTime")) {
        if (auto split = message.split(" "); split.length() >= 2) {
            emit this->jumpToTime(QVariant(split[1]).toDouble());
        }
    }
    property("canNext", m_canNext)
    property("canPrevious", m_canPrevious)
    property("canPause", m_canPause)
    property("canStop", m_canStop)
    property("canPlay", m_canPlay)
    property("canSeek", m_canSeek)
    property("mediaDuration", m_mediaDuration)
    property("currentMediaPosition", m_currentMediaPosition)
    property("currentMediaName", m_currentMediaName)
    property("currentMediaCreator", m_currentMediaCreator)
    property("currentMediaArt", m_currentMediaArt)
    runsignal("next", next)
    runsignal("previous", previous)
    runsignal("pause", pause)
    runsignal("togglePlayPause", togglePlayPause)
    runsignal("stop", stop)
    runsignal("play", play)
    runsignal("reset", reset)
}

void RecastBridge::processBinary(QByteArray message)
{
    QWebSocket *client = qobject_cast<QWebSocket *>(sender());
    m_incomingPayload << message;
    client->sendTextMessage(QStringLiteral("received"));
}

void RecastBridge::socketDisconnected()
{

}
