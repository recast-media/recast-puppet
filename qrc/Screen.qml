import QtQuick 2.5
import QtQuick.Window 2.2
import QtWayland.Compositor 1.0
import QtWebEngine 1.10
import QtWebChannel 1.10
import QtQuick.Layouts 1.5
import QtQuick.Controls 2.5
import com.github.Recast 1.0

WaylandOutput {
    id: output

    property bool isNestedCompositor: Qt.platform.pluginName.startsWith("wayland") || Qt.platform.pluginName === "xcb"

    sizeFollowsWindow: output.isNestedCompositor

    window: Window {
        id: window

        width: 1024
        height: 760
        visible: true

        Image {
            id: background

            anchors.fill: parent
            source: "https://source.unsplash.com/random/%1x%1".arg(window.width).arg(window.height)

            fillMode: Image.PreserveAspectCrop

            smooth: true
        }

        Rectangle {
            anchors {
                bottom: parent.bottom
                left: parent.left
                right: parent.right
            }
            height: Math.round(window.height * (2/5))

            gradient: Gradient {
                GradientStop {
                    position: 1.0
                    color: "black"
                }
                GradientStop {
                    position: 0.0
                    color: "transparent"
                }
            }

            ColumnLayout {
                anchors {
                    verticalCenter: parent.verticalCenter
                    left: parent.left
                    right: parent.right
                    leftMargin: 60
                    rightMargin: 60
                }

                spacing: 10

                Label {
                    color: "white"
                    font.weight: Font.Black
                    font.family: "Noto Sans"
                    font.pixelSize: 128
                    verticalAlignment: Text.AlignBottom
                    text: {
                        return new Date().toLocaleTimeString(undefined, Locale.ShortFormat)
                    }
                }
                RowLayout {
                    Layout.fillWidth: true
                    Label {
                        color: Qt.rgba(1,1,1,0.5)
                        font.family: "Noto Sans"
                        font.pixelSize: 48
                        verticalAlignment: Text.AlignTop
                        text: {
                            return new Date().toLocaleDateString(undefined, Locale.LongFormat)
                        }
                    }
                    Item {
                        Layout.fillWidth: true
                    }
                    Image {
                        width: 32
                        height: 32
                        source: "qrc:/cast-icon.svg"
                        opacity: 0.4
                    }
                    Label {
                        color: Qt.rgba(1,1,1,0.4)
                        font.family: "Noto Sans"
                        font.pixelSize: 32
                        verticalAlignment: Text.AlignTop
                        text: Recast.hostName
                    }
                }
            }
        }
        RecastBridge {
            id: bridge
            WebChannel.id: "recast"
            onReset: {
                fadey.opacity = 0
                viewy.visible = false
                viewy.url = "qrc:///black.html"
            }
            onPreparePayload: {
                fadey.opacity = 1
                viewy.visible = false
            }
            onPayloadLoaded: {
                fadey.opacity = 0
                viewy.visible = true
                viewy.url = "qrc:///black.html"
                viewy.url = "qrc:///app/index.html"
            }
        }
        WebChannel {
            id: channel
            registeredObjects: [bridge]
        }
        WebEngineView {
            id: viewy
            webChannel: channel
            anchors.fill: parent
            visible: false
        }
        Rectangle {
            id: fadey
            opacity: 0
            color: "black"
            anchors.fill: parent
            Behavior on opacity {
                NumberAnimation {
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }
}
